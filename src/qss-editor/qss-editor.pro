include ($$PWD/buildflags.pri)

TARGET = qss-editor
TEMPLATE = subdirs

SUBDIRS += \
        baseclasses \
        main \
        support \
        widgets

support.subdir    = $$PWD/support
main.subdir       = $$PWD/main
widgets.subir     = $$PWD/widgets

baseclasses.depends = support
widgets.depends  =  baseclasses
main.depends = widgets
