TEMPLATE = app
TARGET = qiucus
DESTDIR = $$PWD

include($$PWD/../common.pri)


# ===       Widgets        ===
#PRE_TARGETDEPS += $$BUILD_DIR/qwgtlib/libqwgtlib.a
LIBS += -L$$BUILD_DIR/qwgtlib -lqwgtlib

# ===       Datawidgets    ===
#PRE_TARGETDEPS += $$BUILD_DIR/datawidget/libdatawidget.a
LIBS += -L$$BUILD_DIR/datawidget -ldatawidget

# ===       Dockwidget     ===
#PRE_TARGETDEPS += $$BUILD_DIR/dockwidget/libdockwidget.a
LIBS += -L$$BUILD_DIR/dockwidget -ldockwidget

# ===    Docking system    ===
#PRE_TARGETDEPS += $$BUILD_DIR/qdocksystem/libqdocksystem.a
LIBS += -L$$BUILD_DIR/qdocksystem -lqdocksystem

# ===       Support        ===
#PRE_TARGETDEPS += $$BUILD_DIR/support/libsupport.a
LIBS += -L$$BUILD_DIR/support -lsupport

# ===       Dataentity     ===
#PRE_TARGETDEPS += $$BUILD_DIR/qdelib/libqdelib.a
LIBS += -L$$BUILD_DIR/qdelib -lqdelib

# ===       QScintilla     ===
LIBS += -L$$SOURCE_DIR -lqscintilla2_qt5

# ===         Poppler      ===
unix: PKGCONFIG += poppler-qt5
unix: CONFIG += link_pkgconfig

SOURCES *= $$PWD/*.cpp
HEADERS *= $$PWD/*.hpp

