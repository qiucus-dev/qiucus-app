/*!
 * \file main.cpp
 * \brief
 */

//===    Qt    ===//
#include <QApplication>
#include <QFile>
#include <QXmlStreamReader>


//=== Internal ===//
#include <dockwidget/dockmanager.hpp>
#include <dockwidget/doublepanetabs.hpp>


//=== Sibling  ===//
#include "mainwindow.hpp"
#include "qiucus_application.hpp"


//  ////////////////////////////////////

void h590() {
    { wgt::DoublePaneTabs tabs; }
    { qds::DockManager manager(nullptr); }
}


int main(int argc, char* argv[]) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QiucusApplication a(argc, argv);

#ifdef KBDWIDGET_DEBUG
    dbg::DebugWindow::instance()->show();
#else
    dbg::DebugWindow::instance()->hide();
#endif


    a.readAppSettings();
    a.readApplicationState();
    a.appyApplicationState();
    a.show();

    int appExecRes = a.exec();

    a.saveApplicationState();
    a.saveAppSettings();

    return appExecRes;
}
