#include <datawidget/linkviewer.hpp>
#include <datawidget/noteviewer.hpp>
#include <dockwidget/dockmanager.hpp>
#include <dockwidget/doublepanetabs.hpp>
#include <dockwidget/qds_tab.hpp>
#include <qwgtlib/pdfviewer.hpp>
#include <qwgtlib/textedit/adoceditor.hpp>
#include <qwgtlib/textedit/latexeditor.hpp>
#include <qwgtlib/textedit/markdowneditor.hpp>
#include <qwgtlib/textedit/quicktextedit.hpp>
#include <qwgtlib/textedit/richtexteditor.hpp>
#include <qwgtlib/utility/lodselector.hpp>
#include <qwgtlib/utility/multiselect.hpp>
#include <qwgtlib/webview.hpp>

namespace trash {
void none() {
    { wgt::MultiSelect select; }
    { wgtm::RichTextEditor editor(""); }
    { wgtm::QuickTextEdit edit; }
    { wgtm::PdfViewer view; }
    { dwgt::LinkViewer view; }
    { wgtm::WebView view; }
    { wgtm::LatexEditor edit; }
    { wgt::DoublePaneTabs tabs; }
    { qds::DockManager manager(nullptr); }
    { qds::Tab tab(std::make_unique<QWidget>(), "None"); }
    { LODSelector selector; }
    { wgtm::MarkdownEditor edit; }
    { wgtm::ADOCEditor edit; }
    { dwgt::NoteViewer view(""); }
}
} // namespace trash
