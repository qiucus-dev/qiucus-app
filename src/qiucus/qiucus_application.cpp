#include "qiucus_application.hpp"


#include <QDir>
#include <hdebugmacro/all.hpp>
#include <hsupport/misc.hpp>


QiucusApplication::QiucusApplication(int& argc, char** argv)
    : QApplication(argc, argv)
//  ,
{
    qsrand(static_cast<uint>(QTime::currentTime().msec()));
}


QiucusApplication::~QiucusApplication() {
    spt::removeDirContent(getScratchFolder());
    saveApplicationState();
}


/*!
 * \brief Read state state of the application (currently opened files, root
 * directory for file tree etc.) from the setings folder.
 */
void QiucusApplication::readApplicationState(
    /// Session state file name
    QString name) {
    try {
        settings = jsonFromFile(getSettingsFolder() + name);
        INFO << "Previous application state";
        LOG << settings.dump(4);
    } catch (...) { ERROR << "Cannot read previous application state"; }
}

void QiucusApplication::appyApplicationState() {
    if (getSettings().find("mainwindow") != getSettings().end()) {
        window.setSettings(getSettings()["mainwindow"]);
    }
}

void QiucusApplication::show() {
    window.show();
}


json& QiucusApplication::getSettings() {
    return settings;
}


/*!
 * \brief Save state state of the application (currently opened files, root
 * directory for file tree etc.) into the setings folder
 */
void QiucusApplication::saveApplicationState(
    /// Session state file name
    QString name) {
    settings["mainwindow"] = window.getSettings();
    jsonToFile(getSettings(), getSettingsFolder() + name);

    INFO << "Current application state";
    LOG << settings.dump(4);
}


void QiucusApplication::readAppSettings(QString path, bool relative) {
    QString abs_path;
    if (relative) {
        abs_path = QDir(QDir::currentPath()).absoluteFilePath(path);
    } else {
        abs_path = path;
    }

    if (!QFileInfo(abs_path).exists()) {
        LOG << "Initial config settings do not exist";
    } else {
        try {
            json initSettings = jsonFromFile(abs_path);
            setSettingsFolder(
                QString::fromStdString(initSettings["settings_folder"]));

            INFO << "Initial settings";
            LOG << initSettings.dump(4);
        } catch (...) { ERROR << "Initial app settings are invalid"; }
    }
}

void QiucusApplication::saveAppSettings(QString path, bool relative) {
    QString abs_path;
    if (relative) {
        abs_path = QDir(QDir::currentPath()).absoluteFilePath(path);
    } else {
        abs_path = path;
    }

    json initSettings;
    initSettings["settings_folder"] = getSettingsFolder().toStdString();
    jsonToFile(getSettings(), abs_path);
}
