#pragma once


/*!
 * \file mainwindow.hpp
 */

//===    Qt    ===//
#include <QMainWindow>


//=== Internal ===//
#include <halgorithm/qwidget_cptr.hpp>
#include <hsupport/qjson.hpp>


//  ////////////////////////////////////

// Forward declaration

namespace wgt {
class DoublePaneTabs;
class FileTreeViewer;
} // namespace wgt

class QSplitter;


//  ////////////////////////////////////


/*!
 * \brief Parent class for all types of central widgets for main window
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
  public:
    explicit MainWindow(QWidget* parent = nullptr);
    void    setFileTreeFolder(QString dir);
    QString getFileTreeFolder() const;
    void    setSettings(json settings);
    json    getSettings();

  private:
    spt::qwidget_cptr<wgt::DoublePaneTabs> mainTabs;
    spt::qwidget_cptr<wgt::FileTreeViewer> fileTree;
    spt::qwidget_cptr<QSplitter>           splitter;

    void initSignals();
};
