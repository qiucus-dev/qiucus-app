#ifndef TREEVIEWER_H
#define TREEVIEWER_H

#include "treemodel.h"
#include <dataentity.hpp>
#include <datawidget.hpp>

#include <QFile>
#include <QGridLayout>
#include <QModelIndex>
#include <QTreeView>

namespace dwgt {
class TreeViewer : public DataWidget
{
  public:
    TreeViewer();

  private:
    QTreeView*   view  = nullptr;
    TreeModel*   model = nullptr;
    QGridLayout* mainLayout;
};
} // namespace dwgt

#endif // TREEVIEWER_H
