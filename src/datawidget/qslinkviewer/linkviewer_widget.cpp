#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QSplitter>
#include <QTextEdit>
#include <QToolBox>
#include <QVBoxLayout>
#include <QtCore/QVariant>

#include <hsupport/icons.hpp>

#include "linkviewer_widget.hpp"

LinkViewer_widget::LinkViewer_widget(QWidget* parent)
    : QWidget(parent)
    , widgets {
        {new QPushButton(this)}, // urlPreview_pbtn
        {new QLineEdit(this)}, // urlEdit_edit
        {new wgtm::QuickTextEdit(this)}, // description_edit
        {new wgtm::QuickTextEdit(this)}, // metaNote_edit
        {new wgt::MultiSelect(this)}, // linkTags_select
        {new wgt::MultiSelect(this)}, // metaTags_select
        {new wgtm::WebView(this)},  // urlPreview
        {new QPushButton(this)}, // toggleEdit_pbtn
    }, ui {
        {new QToolBox(this)}, // urlEdit_toolbox
        {new QSplitter(this)}, // mainSplitter
        {new QVBoxLayout}, // mainLayout
        {new QGridLayout}, // urlEdit_layout
        {new QFrame(this)}, // urlEdit_frame
        {new QGridLayout}, // urlPreview_layout
        {new QFrame(this)}, // urlPreview_frame
    }
    //
{
}


void LinkViewer_widget::setupUi() {
    setLayout(ui.mainLayout);
    ui.mainLayout->setParent(this);

    //#=== Main setup
    ui.mainLayout->addWidget(ui.mainSplitter);
    ui.mainSplitter->setChildrenCollapsible(false);
    ui.mainSplitter->addWidget(ui.urlEdit_frame);
    ui.mainSplitter->addWidget(ui.urlPreview_frame);
    ui.mainSplitter->setStretchFactor(1, 4);

    //#=== Link edit setup
    ui.urlEdit_frame->setLayout(ui.urlEdit_layout);
    ui.urlEdit_layout->setParent(ui.urlEdit_frame);
    ui.urlEdit_layout->setContentsMargins(spt::zeroMargins());

    ui.urlEdit_layout->addWidget(ui.urlEdit_toolbox, 2, 1);
    ui.urlEdit_layout->addWidget(widgets.urlEditToggle_pbtn, 1, 1);
    widgets.urlEditToggle_pbtn->setCheckable(true);
    widgets.urlEditToggle_pbtn->setText("Hide");

    ui.urlEdit_toolbox->addItem(widgets.description_edit, "Description");
    ui.urlEdit_toolbox->addItem(widgets.metaNote_edit, "Meta note");
    ui.urlEdit_toolbox->addItem(widgets.metaTags_select, "Meta tags");
    ui.urlEdit_toolbox->addItem(widgets.linkTags_select, "Link tags");


    //#=== Url preview setup
    ui.urlPreview_frame->setLayout(ui.urlPreview_layout);
    ui.urlPreview_layout->setParent(ui.urlPreview_frame);
    ui.urlPreview_frame->setContentsMargins(spt::zeroMargins());
    ui.urlPreview_layout->setContentsMargins(spt::zeroMargins());

    ui.urlPreview_layout->addWidget(widgets.urlEdit_edit, 1, 1);
    ui.urlPreview_layout->addWidget(widgets.urlPreview_pbtn, 1, 2);
    ui.urlPreview_layout->addWidget(widgets.urlPreview, 2, 1, 1, 2);
    ui.urlEdit_layout->setRowStretch(2, 10);

    widgets.urlPreview_pbtn->setText("Preview");
}

void LinkViewer_widget::setupSignals() {
    connect(
        widgets.urlEditToggle_pbtn.get(), &QPushButton::pressed, [&]() {
            ui.urlEdit_toolbox->hide();
            widgets.urlEditToggle_pbtn->setText("Show");
        });

    connect(
        widgets.urlEditToggle_pbtn.get(), &QPushButton::released, [&]() {
            ui.urlEdit_toolbox->show();
            widgets.urlEditToggle_pbtn->setText("Hide");
        });

    connect(widgets.urlPreview_pbtn, &QPushButton::clicked, [this]() {
        widgets.urlPreview->setUrl(widgets.urlEdit_edit->text());
    });
}
