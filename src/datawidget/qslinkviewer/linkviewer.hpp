#ifndef LinkViewer_H
#define LinkViewer_H

#include <QDebug>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QSplitter>
#include <QStringList>
#include <QTextLine>
#include <QVBoxLayout>
#include <QWebEngineView>
#include <QWidget>

#include <memory>
#include <variant>

#include <qdelib/qslink.hpp>

#include "../datawidget/datawidget.hpp"
#include "../datawidget/tagsviewer.hpp"
#include "linkviewer_widget.hpp"


namespace dwgt {
class LinkViewer
    : public DataWidget
    , public LinkViewer_widget
    , public WidgetInterface
    , public wgt::KeyboardSupport
{
  public:
    explicit LinkViewer(QWidget* parent = nullptr);
    qde::QSLink* getLink();
    qde::QSLink* getLink() const;
    void         setLink(qde::QSLink* value);
    QStringList  getTags();


  private:
    std::variant<std::unique_ptr<qde::QSLink>, qde::QSLink*> link;

    //#== WidgetInterface interface
  protected:
    void initUI() override;
    void updateUI() override;
    void initSignals() override;
    void updateContent() override;

    //#== WidgetManageable interface
  public slots:
    void openFile(QString path) override;
    void saveFile() override;
    bool isEmpty() const override;

  protected:
    void    writeFile(QFile& file) override;
    void    readFile(QFile& file) override;
    QString getExtension() const override;

    //#== DataWidget interface
  protected:
    bool    isOwner() const override;
    QString getWidgetName() const override;

    //#== KeyboardSupport interface
  protected:
    void keyPressEvent(QKeyEvent* event) override;
    bool processKeyEvent(wgt::KeyEvent& event) override;
    void initShortcuts() override;
};


} // namespace dwgt

#endif // LinkViewer_H
