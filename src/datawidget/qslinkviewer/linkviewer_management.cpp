#include "linkviewer.hpp"
#include <hdebugmacro/all.hpp>

namespace dwgt {
void LinkViewer::readFile(QFile& file) {
    QFileInfo info(file);
    getLink()->readFile(info.path());
    currentFile = info.path();
}

void LinkViewer::writeFile(QFile& file) {
    QFileInfo info(file);
    this->updateContent();
    getLink()->writeFile(info.path());
}

void LinkViewer::openFile(QString path) {
    DWGT_FUNC_BEGIN

    if (QFile(path).exists()) {
        currentFile = path;
        link        = std::make_unique<qde::QSLink>();
        getLink()->readFile(path);
        this->updateUI();
    }

    DWGT_FUNC_END
}

void LinkViewer::saveFile() {
    DWGT_FUNC_BEGIN

    updateContent();
    if (isOwner()) {
        getLink()->saveFile(currentFile);
    }

    DWGT_FUNC_END
}


} // namespace dwgt
