#include "linkviewer.hpp"

namespace dwgt {
LinkViewer::LinkViewer(QWidget* parent) : LinkViewer_widget(parent) {
    this->initUI();
    this->initSignals();
    this->initShortcuts();
}

qde::QSLink* LinkViewer::getLink() {
    if (link.index() == 0) {
        return std::get<0>(link).get();
    } else {
        return std::get<1>(link);
    }
}

qde::QSLink* LinkViewer::getLink() const {
    if (link.index() == 0) {
        return std::get<0>(link).get();
    } else {
        return std::get<1>(link);
    }
}


void LinkViewer::setLink(qde::QSLink* value) {
    link = value;
}


QStringList LinkViewer::getTags() {
    return QStringList();
    //    return tagsViewer->getTags();
}

bool LinkViewer::isEmpty() const {
    return getLink() == nullptr;
}

QString LinkViewer::getWidgetName() const {
    return "LinkViewer";
}

void LinkViewer::keyPressEvent(QKeyEvent* event) {
    KBD_FUNC_BEGIN


    wgt::KeyEvent keyEvent(event);

    KBD_INFO << "LinkViewer key press event"
             << keyEvent.getSequence().toString();

    if (!processKeyEvent(keyEvent)) {
        QWidget::keyPressEvent(event);
    }

    KBD_FUNC_END
    DEBUG_FINALIZE_WINDOW
}

bool LinkViewer::processKeyEvent(wgt::KeyEvent& event) {
    KBD_FUNC_BEGIN

    QString            command = shortcuts.getCommand(event.getSequence());
    wgt::KBDParameters parameters;

    KBD_FUNC_END
    return executeCommand(command, parameters);
}

void LinkViewer::initShortcuts() {
    shortcuts.addBinding("ctrl+s", "save-content");
    keyboardActions["save-content"] =
        [&](wgt::KBDParameters& internalParameters,
            json&               userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)
        KBD_LOG << "LinkViewer save-content";
        this->saveFile();
        return true;
    };
}

QString LinkViewer::getExtension() const {
    return "qslink";
}

bool LinkViewer::isOwner() const {
    return link.index() == 0;
}

} // namespace dwgt
