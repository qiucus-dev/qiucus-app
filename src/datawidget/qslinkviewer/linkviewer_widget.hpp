#ifndef LINKVIEWER_WIDGET_H
#define LINKVIEWER_WIDGET_H

#include <QWidget>

#include <halgorithm/qwidget_cptr.hpp>
#include <qwgtlib/base/widgetinterface.hpp>
#include <qwgtlib/textedit/quicktextedit.hpp>
#include <qwgtlib/utility/multiselect.hpp>
#include <qwgtlib/webview.hpp>

class QToolBox;
class QSplitter;
class QVBoxLayout;
class QFrame;
class QPushButton;
class QLineEdit;
class QGridLayout;


class LinkViewer_widget : public QWidget
{
    Q_OBJECT
  public:
    LinkViewer_widget(QWidget* parent = nullptr);

  protected:
    struct {
        spt::qwidget_cptr<QPushButton>         urlPreview_pbtn;
        spt::qwidget_cptr<QLineEdit>           urlEdit_edit;
        spt::qwidget_cptr<wgtm::QuickTextEdit> description_edit;
        spt::qwidget_cptr<wgtm::QuickTextEdit> metaNote_edit;
        spt::qwidget_cptr<wgt::MultiSelect>    linkTags_select;
        spt::qwidget_cptr<wgt::MultiSelect>    metaTags_select;
        spt::qwidget_cptr<wgtm::WebView>       urlPreview;
        spt::qwidget_cptr<QPushButton>         urlEditToggle_pbtn;
    } widgets;

  private:
    struct {
        spt::qwidget_cptr<QToolBox>    urlEdit_toolbox;
        spt::qwidget_cptr<QSplitter>   mainSplitter;
        spt::qwidget_cptr<QVBoxLayout> mainLayout;
        spt::qwidget_cptr<QGridLayout> urlEdit_layout;
        spt::qwidget_cptr<QFrame>      urlEdit_frame;
        spt::qwidget_cptr<QGridLayout> urlPreview_layout;
        spt::qwidget_cptr<QFrame>      urlPreview_frame;
    } ui;

  protected:
    void setupUi();
    void setupSignals();

  private:
    int urlEditWidth = 0;
};


#endif // LINKVIEWER_WIDGET_H
