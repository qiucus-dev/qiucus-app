#include "linkviewer.hpp"
#include <hdebugmacro/all.hpp>


namespace dwgt {
void LinkViewer::initUI() {
    this->setupUi();
    this->updateUI();
}

void LinkViewer::updateContent() {
    DWGT_FUNC_BEGIN

    if (!isEmpty()) {
        qde::QSLink* _link = getLink();

        _link->setURL(widgets.urlEdit_edit->text());
        _link->setMetaDescription(widgets.description_edit->getText());
        _link->setMetaNote(widgets.metaNote_edit->getText());
        _link->setLinkTags(
            widgets.linkTags_select->getSelectedList().toStringList());
        _link->setMetaTags(
            widgets.metaTags_select->getSelectedList().toStringList());
    }

    DWGT_FUNC_END
}

void LinkViewer::updateUI() {
    DWGT_FUNC_BEGIN

    if (!isEmpty()) {
        qde::QSLink* _link = getLink();

        widgets.metaTags_select->setSelectedList(_link->getMetaTags());
        widgets.linkTags_select->setSelectedList(_link->getLinkTags());
        widgets.description_edit->setText(_link->getMetaDescription());
        widgets.metaNote_edit->setText(_link->getMetaNote());
        widgets.urlEdit_edit->setText(_link->getURL().toString());

        widgets.urlPreview->setUrl(_link->getURL());
    }

    DWGT_FUNC_END
}

void LinkViewer::initSignals() {
    LinkViewer_widget::setupSignals();

    connect(
        widgets.description_edit,
        &wgtm::QuickTextEdit::saveRequested,
        this,
        &LinkViewer::saveFile);
}
} // namespace dwgt
