#include "mindmapview.hpp"

MindMapView::MindMapView() {
    qDebug() << "MindMapView constructed";
}

MindMapView::~MindMapView() {
    qDebug() << "Destroying MindMapView";
}

std::shared_ptr<QGraphicsScene> MindMapView::getScene() const {
    return scene;
}

void MindMapView::setScene(const std::shared_ptr<QGraphicsScene>& value) {
    qDebug() << "MindMapView::setScene has been called";
    scene = value;
}

QList<QGraphicsItem*> MindMapView::getAllItems() {
}

// void MindMapView::reexportScene() {
//  foreach (QGraphicsItem* item, scene.get()->items()) {
//    scene.get()->removeItem(item);
//  }

//  foreach (QGraphicsItem* item, this->getAllItems()) {

//  }
//}
