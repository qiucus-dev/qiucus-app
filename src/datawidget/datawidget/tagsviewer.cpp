#include "tagsviewer.hpp"

TagsViewer::TagsViewer(QWidget* parent) : QWidget(parent) {
}

void TagsViewer::setTags(QStringList tags) {
    tagsList = tags;
}

QStringList TagsViewer::getTags() {
    return tagsList;
}
