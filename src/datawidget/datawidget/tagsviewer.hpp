#ifndef TAGSVIEWER_H
#define TAGSVIEWER_H

#include <QStringList>
#include <QWidget>

class TagsViewer : public QWidget
{
    Q_OBJECT
  public:
    explicit TagsViewer(QWidget* parent = nullptr);
    void        setTags(QStringList tags);
    QStringList getTags();

  private:
    QStringList tagsList;
};

#endif // TAGSVIEWER_H
