HEADERS *= \
    $$PWD/datawidget.hpp \
    $$PWD/tagsviewer.hpp \
    $$PWD/dwgt_debug_macro.hpp

SOURCES *= \
    $$PWD/datawidget.cpp \
    $$PWD/tagsviewer.cpp
