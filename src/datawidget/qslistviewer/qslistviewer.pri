SOURCES += \
    $$PWD/matrixviewer.cpp \
    $$PWD/vectorviewer.cpp \
    $$PWD/listmodel.cpp \
    $$PWD/tablemodel.cpp \
    $$PWD/tableviewer.cpp

HEADERS += \
    $$PWD/vectorviewer.hpp \
    $$PWD/matrixviewer.hpp \
    $$PWD/listmodel.hpp \
    $$PWD/tablemodel.hpp \
    $$PWD/tableviewer.hpp
