#include "tableviewer.hpp"

namespace dwgt {
TableViewer::TableViewer() {
    matrixViewer = new MatrixViewer;
    tableModel   = new TableModel;
    matrixViewer->setModel(tableModel);

    vectorViewer = new VectorViewer;
    listModel    = new ListModel;
    vectorViewer->setModel(listModel);

    //  vectorViewer->setViewMode(QListView::IconMode);

    savePBtn = new QPushButton;

    mainLayout = new QHBoxLayout;
    //    mainLayout->addWidget(savePBtn);
    //    mainLayout->addWidget(vectorViewer);
    //    mainLayout->addWidget(matrixViewer);
    //    this->setLayout(mainLayout);

    //    list = new dent::QSTable;

    //    connect(savePBtn, &QPushButton::clicked, [this]() {
    //        this->saveTestFile("qstable");
    //    });
}

void TableViewer::writeFile(QString path) {
    list->writeFile(path);
}
} // namespace dwgt
