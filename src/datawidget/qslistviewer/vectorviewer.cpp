#include "vectorviewer.hpp"

VectorViewer::VectorViewer(QWidget* parent) : QListView(parent) {
    connect(this, &VectorViewer::clicked, [this](QModelIndex index) {
        emit cellSelected(listModel->at(index));
    });
}

void VectorViewer::setModel(QAbstractItemModel* model) {
    QAbstractItemView::setModel(model);
    this->listModel = dynamic_cast<ListModel*>(model);
}
