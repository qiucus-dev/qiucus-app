#ifndef CALENDARMODEL_HPP
#define CALENDARMODEL_HPP

/*!
 * \file calendarmodel.hpp
 * \brief
 */

//===    STL   ===//
#include <memory>

//=== Internal ===//
#include <qsevent/qsevent.hpp>


//  ////////////////////////////////////


namespace dwgt {


class CalendarModel
{
  public:
    struct Day {
        std::vector<dent::QSEvent*> events;
    };

    struct Month {
        std::vector<std::unique_ptr<Day>> days;
    };

    CalendarModel();

  private:
    std::vector<Month*> months;
};
} // namespace dwgt

#endif // CALENDARMODEL_HPP
