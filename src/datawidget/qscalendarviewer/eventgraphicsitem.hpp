#ifndef EVENTGRAPHICSITEM_HPP
#define EVENTGRAPHICSITEM_HPP

#include <QGraphicsItem>


class EventGraphicsItem : extends QGraphicsItem
{
  public:
    EventGraphicsItem();

    // QGraphicsItem interface
  public:
    QRectF boundingRect() const;
    void   paint(
          QPainter*                       painter,
          const QStyleOptionGraphicsItem* option,
          QWidget*                        widget);
};

#endif // EVENTGRAPHICSITEM_HPP
