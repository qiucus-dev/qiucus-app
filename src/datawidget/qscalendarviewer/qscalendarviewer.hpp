#ifndef QSCALENDARVIEWER_HPP
#define QSCALENDARVIEWER_HPP

#include "calendarmodel.hpp"
#include "calendarmodelviewer.hpp"
#include <QWidget>
#include <datawidget.hpp>
#include <memory>
#include <qwidget_cptr.hpp>

namespace dwgt {
class QSCalendarViewer
    : extends QWidget
    , implements DataWidget
{
    Q_OBJECT
  public:
    explicit QSCalendarViewer(QWidget* parent = nullptr);

  private:
    std::unique_ptr<CalendarModel>         calendarModel;
    spt::qwidget_cptr<CalendarModelViewer> calendarModelViewer;
};
} // namespace dwgt

#endif // QSCALENDARVIEWER_HPP
