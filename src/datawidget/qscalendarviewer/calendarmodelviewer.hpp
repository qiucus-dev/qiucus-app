#ifndef AGENDAVIEWER_HPP
#define AGENDAVIEWER_HPP

#include "calendarmodel.hpp"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QVBoxLayout>
#include <QWidget>
#include <qwidget_cptr.hpp>

namespace dwgt {
class CalendarModelViewer : public QWidget
{
    Q_OBJECT
  public:
    explicit CalendarModelViewer(QWidget* parent = nullptr);

  private:
    spt::qwidget_cptr<QVBoxLayout>   layout;
    spt::qwidget_cptr<QGraphicsView> view;
    CalendarModel*                   model;
};
} // namespace dwgt

#endif // AGENDAVIEWER_HPP
