#ifndef NOTEVIEWER_WIDGET_HPP
#define NOTEVIEWER_WIDGET_HPP

/*!
 * \file noteviewer_widget.hpp
 * \brief
 */

//===    Qt    ===//
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QFrame>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QSplitter>
#include <QStackedWidget>
#include <QTextEdit>
#include <QToolBox>
#include <QVBoxLayout>
#include <QVariant>
#include <QWidget>

//=== Internal ===//
#include <hdebugmacro/all.hpp>
#include <qwgtlib/base/lodsupport.hpp>
#include <qwgtlib/textedit/adoceditor.hpp>
#include <qwgtlib/textedit/codeeditor.hpp>
#include <qwgtlib/textedit/latexeditor.hpp>
#include <qwgtlib/textedit/quicktextedit.hpp>
#include <qwgtlib/textedit/richtexteditor.hpp>
#include <qwgtlib/utility/lodselector.hpp>
#include <qwgtlib/utility/multiselect.hpp>


//  ////////////////////////////////////


class NoteViewer_widget : public QWidget
{
    Q_OBJECT
  public:
    NoteViewer_widget(QString iconsDir, QWidget* parent = nullptr);
    ~NoteViewer_widget() override = default;

  protected:
    void            setupUI();
    wgtm::TextEdit* getActiveEditor();


    // UI Elements
  protected:
    struct {
        spt::qwidget_cptr<QComboBox>            noteType_selector;
        spt::qwidget_cptr<wgt::MultiSelect>     noteTags_selector;
        spt::qwidget_cptr<wgt::MultiSelect>     metaTags_selector;
        spt::qwidget_cptr<wgtm::QuickTextEdit>  descriptionEditor;
        spt::qwidget_cptr<wgtm::QuickTextEdit>  metaNoteEditor;
        spt::qwidget_cptr<wgtm::RichTextEditor> richTextEditor;
        spt::qwidget_cptr<wgtm::CodeEditor>     codeEditor;
        spt::qwidget_cptr<wgtm::ADOCEditor>     adocEditor;
    } widgets;

    void setAsciidocEditor();
    void setRichTextEditor();
    void setCodeEditor();

  private:
    struct {
        spt::qwidget_cptr<QToolBox>    metaEditor_toolbox;
        spt::qwidget_cptr<QSplitter>   mainSplitter;
        spt::qwidget_cptr<QFrame>      textEdit_frame;
        spt::qwidget_cptr<QHBoxLayout> textEdit_layout;
        spt::qwidget_cptr<QHBoxLayout> mainLayout;
    } ui;

    std::variant<
        wgtm::RichTextEditor*,
        wgtm::ADOCEditor*,
        wgtm::CodeEditor*>
        activeEditor;
};


#endif // NOTEVIEWER_WIDGET_HPP
