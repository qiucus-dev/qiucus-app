#include "noteviewer.hpp"

/*!
 * \file noteviewer.cpp
 * \brief
 */


//=== Internal ===//
#include <hdebugmacro/all.hpp>


//  ////////////////////////////////////


namespace dwgt {
NoteViewer::NoteViewer(QString iconsDir, QWidget* parent)
    : NoteViewer_widget(iconsDir, parent)
//  ,
{
    this->initUI();
    this->initSignals();
    this->updateUI();
}


/// \brief Get pointer to note that is currently being edited
/// \note Pointer returned oblivoius to whether LinkViewer is owner or not
qde::QSNote* NoteViewer::getNote() {
    if (note.index() == 0) {
        return std::get<0>(note).get();
    } else {
        return std::get<1>(note);
    }
}

/// \brief Get pointer to note that is currently being edited
/// \note Pointer returned oblivoius to whether LinkViewer is owner or not
qde::QSNote* NoteViewer::getNote() const {
    if (note.index() == 0) {
        return std::get<0>(note).get();
    } else {
        return std::get<1>(note);
    }
}


/// \brief Set current note being edited by this widget.
void NoteViewer::setNote(qde::QSNote* value) {
    note = value;
}


void NoteViewer::initUI() {
    this->setupUI();
}


void NoteViewer::updateUI() {
    DWGT_FUNC_BEGIN

    widgets.descriptionEditor->setText(getNote()->getMetaDescription());
    widgets.metaNoteEditor->setText(getNote()->getMetaNote());
    widgets.metaTags_selector->setSelectedList(getNote()->getMetaTags());
    widgets.noteTags_selector->setSelectedList(getNote()->getNoteTags());

    using Type = qde::QSNote::NoteType;

    INFO << "Note content";
    LOG << getNote()->toDebugString();

    switch (getNote()->getNoteType()) {
        case Type::RichText: {
            setRichTextEditor();
            getActiveEditor()->setText(getNote()->getNoteText());
            widgets.noteType_selector->setCurrentIndex(
                widgets.noteType_selector->findText("RichText"));
        } break;
        case Type::Asciidoctor: {
            setAsciidocEditor();
            getActiveEditor()->setText(getNote()->getNoteText());
            widgets.noteType_selector->setCurrentIndex(
                widgets.noteType_selector->findText("Asciidoctor"));
        } break;
        default: { DEBUG_WARN_IF_CALLED("Unhandled note type") }
    }


    DWGT_FUNC_END
}

void NoteViewer::updateContent() {
    getNote()->setMetaDescription(widgets.descriptionEditor->getText());
    getNote()->setMetaNote(widgets.metaNoteEditor->getText());
    getNote()->setNoteText(getActiveEditor()->getText());
}


void NoteViewer::initSignals() {
    connect(
        widgets.noteType_selector,
        &QComboBox::currentTextChanged,
        [this](QString newType) {
            Q_UNUSED(newType)
            wgtm::TextEdit* oldEditor = getActiveEditor();
            updateContent();
            oldEditor->clean();
            updateUI();
        });
}


void NoteViewer::saveFile() {
    this->updateContent();
    if (currentFile.isEmpty()) {
        currentFile = askPath();
    }
    INFO << "Writing note to" << currentFile;
    writeFile(currentFile);
}


void NoteViewer::openFile(QString path) {
    DWGT_FUNC_BEGIN

    if (QFile(path).exists()) {
        currentFile = path;
        note        = std::make_unique<qde::QSNote>();
        getNote()->readFile(path);
        this->updateUI();
    }

    DWGT_FUNC_END
}


void NoteViewer::writeFile(QString path) {
    getNote()->writeFile(path);
}


bool NoteViewer::isEmpty() const {
    return getNote() == nullptr;
}


QString NoteViewer::getWidgetName() const {
    return "NoteViewer";
}


QString NoteViewer::getExtension() const {
    return "qsnote";
}


} // namespace dwgt
