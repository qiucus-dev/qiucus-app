#ifndef NOTEVIEWER_H
#define NOTEVIEWER_H

/*!
 * \file noteviewer.hpp
 * \brief
 */

//===    Qt    ===//
#include <QGridLayout>
#include <QHBoxLayout>
#include <QObject>
#include <QPushButton>
#include <QWidget>

//===    STL   ===//
#include <memory>
#include <variant>

//=== Internal ===//
#include <hdebugmacro/all.hpp>
#include <qdelib/qsnote.hpp>

//=== Sibling  ===//
#include "../datawidget/datawidget.hpp"
#include "../datawidget/tagsviewer.hpp"
#include "noteviewer_widget.hpp"


//  ////////////////////////////////////

// Forward declaration

namespace qde {
class QSNote;
}


//  ////////////////////////////////////


namespace dwgt {
class NoteViewer
    : public NoteViewer_widget
    , public DataWidget
    , public WidgetInterface
{
    Q_OBJECT
  public:
    explicit NoteViewer(QString iconsDir, QWidget* parent = nullptr);

    qde::QSNote* getNote();
    qde::QSNote* getNote() const;
    void         setNote(qde::QSNote* value);

  private:
    std::variant<std::unique_ptr<qde::QSNote>, qde::QSNote*> note;
    wgtm::TextEdit* noteTextEdit();


    //#== WidgetInterface interface
  protected:
    void    updateUI() override;
    void    initUI() override;
    void    updateContent() override;
    void    initSignals() override;
    QString getExtension() const override;

    //#== WidgetManageable interface
  public slots:
    void    saveFile() override;
    void    openFile(QString path) override;
    void    writeFile(QString path) override;
    bool    isEmpty() const override;
    QString getWidgetName() const override;
};

} // namespace dwgt

#endif // NOTEVIEWER_H
