#include "noteviewer_widget.hpp"


void NoteViewer_widget::setupUI() {
    DEBUG_WARN_IF_CALLED("Calling qsnoteviewer setup ui");
    setLayout(ui.mainLayout);
    ui.mainLayout->setParent(this);
    ui.mainLayout->addWidget(ui.mainSplitter);

    ui.mainSplitter->addWidget(ui.textEdit_frame);
    ui.mainSplitter->addWidget(ui.metaEditor_toolbox);
    ui.mainSplitter->setChildrenCollapsible(false);
    ui.mainSplitter->setStretchFactor(0, 4);

    ui.metaEditor_toolbox->addItem(widgets.noteTags_selector, "Note tags");
    ui.metaEditor_toolbox->addItem(widgets.noteType_selector, "Note type");
    ui.metaEditor_toolbox->addItem(widgets.metaTags_selector, "Meta tags");
    ui.metaEditor_toolbox->addItem(
        widgets.descriptionEditor, "Description");
    ui.metaEditor_toolbox->addItem(widgets.metaNoteEditor, "Meta note");


    ui.textEdit_frame->setContentsMargins(spt::zeroMargins());
    ui.textEdit_layout->setContentsMargins(spt::zeroMargins());
    ui.textEdit_frame->setLayout(ui.textEdit_layout);
    ui.textEdit_layout->setParent(ui.textEdit_frame);
    ui.textEdit_layout->addWidget(widgets.adocEditor);
    ui.textEdit_layout->addWidget(widgets.richTextEditor);
    ui.textEdit_layout->addWidget(widgets.codeEditor);
    setAsciidocEditor();
}


NoteViewer_widget::NoteViewer_widget(QString iconsDir, QWidget* parent)
    : QWidget(parent) //
    , widgets{
        {new QComboBox(this)},             // noteType_selector
        {new wgt::MultiSelect(this)},      // noteTags_selector
        {new wgt::MultiSelect(this)},      // metaTags_selector
        {new wgtm::QuickTextEdit(this)},   // descriptionEditor
        {new wgtm::QuickTextEdit(this)},   // metaNoteEditor
        {new wgtm::RichTextEditor(iconsDir,this)},  // richTextEditor
        {new wgtm::CodeEditor(this)},
        {new wgtm::ADOCEditor(this)},      // adocEditor
    }, ui{
        {new QToolBox(this)},    // metaEditor_toolbox;
        {new QSplitter(this)},   // mainSplitter;
        {new QFrame(this)},      // textEdit_frame;
        {new QHBoxLayout}, // textEdit_layout;
        {new QHBoxLayout}, // mainLayout
    }
    //
{
}

void NoteViewer_widget::setAsciidocEditor() {
    activeEditor = widgets.adocEditor.get();
    widgets.richTextEditor->hide();
    widgets.codeEditor->hide();
    widgets.adocEditor->show();
}

void NoteViewer_widget::setRichTextEditor() {
    activeEditor = widgets.richTextEditor.get();
    widgets.richTextEditor->show();
    widgets.codeEditor->hide();
    widgets.adocEditor->hide();
}

void NoteViewer_widget::setCodeEditor() {
    activeEditor = widgets.codeEditor.get();
    widgets.richTextEditor->hide();
    widgets.codeEditor->show();
    widgets.adocEditor->hide();
}

wgtm::TextEdit* NoteViewer_widget::getActiveEditor() {
    switch (activeEditor.index()) {
        case 0: {
            return std::get<0>(activeEditor);
        }
        case 1: {
            return std::get<1>(activeEditor);
        }
        case 2: {
            return std::get<2>(activeEditor);
        }
    }
}
