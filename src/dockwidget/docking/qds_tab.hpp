#pragma once

#include <QWidget>
#include <memory>
#include <qdocksystem/tab.hpp>

namespace qds {
class Tab : public ads::Tab
{
  public:
    Tab(std::unique_ptr<QWidget> _widget, QString _title);
    Tab(std::unique_ptr<QWidget> _widget, std::unique_ptr<QWidget> _title);
};
} // namespace qds
