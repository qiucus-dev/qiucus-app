#pragma once

#include <qdocksystem/dockmanager.hpp>

namespace qds {
class DockManager : public ads::DockManager
{
  public:
    DockManager(QWidget* parent);
};
} // namespace qds
