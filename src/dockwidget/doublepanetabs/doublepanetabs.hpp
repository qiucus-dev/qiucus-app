#ifndef DOUBLEPANETABS_HPP
#define DOUBLEPANETABS_HPP

/*!
 * \file doublepanetabs.hpp
 * \brief
 */

//===    Qt    ===//
#include <QSplitter>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>

//===   Other  ===//
#include <vector>

//=== Internal ===//
#include <hdebugmacro/all.hpp>


//=== Sibling  ===//
#include "../workspace/workspace.hpp"
#include "tabwidget.hpp"
#include <qwgtlib/base/widgetinterface.hpp>


//  ////////////////////////////////////

// Macro declaration

#ifdef DPNWIDGET_DEBUG
#    define DPN_LOG DebugLogger::log()
#    define DPN_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define DPN_FUNC_END DEBUG_FUNCTION_END
#    define DPN_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
#else
#    define DPN_LOG VOID_LOG
#    define DPN_FUNC_BEGIN
#    define DPN_FUNC_END
#    define DPN_FUNC_RET(message)
#endif

//  ////////////////////////////////////


namespace wgt {
class DoublePaneTabs
    : public QWidget
    , public wgtm::WidgetManageable
    , public WidgetInterface
    , public KeyboardSupport
{
    Q_OBJECT
  public:
    explicit DoublePaneTabs(QWidget* parent = nullptr);
    ~DoublePaneTabs();


  private:
    QTextEdit*   addressField;
    QSplitter*   splitter;
    QVBoxLayout* mainLayout;
    TabWidget*   leftTabs;
    TabWidget*   rightTabs;

    void initUI() override;
    void initSignals() override;
    void updateUI() override;
    void moveToOtherField(
        TabWidget* source,
        TabWidget* destination,
        int        sourceIndex);
    void moveTabToOtherPane(int index);

    TabWidget*              selectedPane;
    Workspace*              getCurrentWorkspace() const;
    Workspace*              getWorkspace(TabWidget* pane, int index);
    std::vector<Workspace*> getLeftWorkspaces() const;
    std::vector<Workspace*> getRightWorkspaces() const;

    // WidgetManageable interface
  public slots:
    void    saveFile() override;
    void    openFile(QString path) override;
    void    newFile() override;
    QString getWidgetName() const override;

    // QWidget interface
  protected:
    void keyPressEvent(QKeyEvent* event) override;

    // WidgetInterface interface
  protected:
    void initShortcuts() override;
    bool processKeyEvent(KeyEvent& event) override;
    bool passDown(KeyEvent& event) override;
};
} // namespace wgt

#endif // DOUBLEPANETABS_HPP
