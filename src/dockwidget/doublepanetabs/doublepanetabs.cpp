#include "doublepanetabs.hpp"

namespace wgt {
DoublePaneTabs::DoublePaneTabs(QWidget* parent) : QWidget(parent) {
    initUI();
    initSignals();
    initShortcuts();
    updateUI();
}

DoublePaneTabs::~DoublePaneTabs() {
    INFO << "Deleting widget manageanble";
    LOG << "Name:" << this->getWidgetName();
}


void DoublePaneTabs::initUI() {
    addressField = new QTextEdit(this);
    splitter     = new QSplitter(this);
    mainLayout   = new QVBoxLayout(this);
    leftTabs     = new TabWidget(splitter);
    rightTabs    = new TabWidget(splitter);
    mainLayout->setContentsMargins(spt::equalMargins(2));
    this->setContentsMargins(spt::equalMargins(2));
    //    splitter->setOrientation(Qt::Vertical);

    leftTabs->setMovable(true);
    rightTabs->setMovable(true);

    splitter->addWidget(leftTabs);
    splitter->addWidget(rightTabs);
    mainLayout->addWidget(splitter);
    mainLayout->addWidget(addressField);
    addressField->setMaximumHeight(28);

    setLayout(mainLayout);
    rightTabs->hide();
    selectedPane = leftTabs;

    leftTabs->setTabsClosable(true);
    rightTabs->setTabsClosable(true);

    leftTabs->setMinimumSize(0, 0);
    rightTabs->setMinimumSize(0, 0);
    this->setMinimumSize(0, 0);
}


void DoublePaneTabs::initSignals() {
    connect(
        leftTabs,
        &TabWidget::tabBarLeftMouseDblClicked,
        [&](int tabIndex) {
            moveToOtherField(leftTabs, rightTabs, tabIndex);
        });

    connect(
        rightTabs,
        &TabWidget::tabBarLeftMouseDblClicked,
        [&](int tabIndex) {
            moveToOtherField(rightTabs, leftTabs, tabIndex);
        });

    connect(
        leftTabs, &TabWidget::tabBarLeftMouseClicked, [&](int tabIndex) {
            selectedPane = leftTabs;
            addressField->setText(
                getWorkspace(leftTabs, tabIndex)->getFile());
        });

    connect(
        rightTabs, &TabWidget::tabBarLeftMouseClicked, [&](int tabIndex) {
            selectedPane = rightTabs;
            addressField->setText(
                getWorkspace(rightTabs, tabIndex)->getFile());
        });

    connect(leftTabs, &TabWidget::tabCloseRequested, [&](int tabIndex) {
        leftTabs->getWorkspace(tabIndex)->saveFile();
        leftTabs->getWorkspace(tabIndex)->deleteLater();
        leftTabs->removeTab(tabIndex);
    });

    connect(rightTabs, &TabWidget::tabCloseRequested, [&](int tabIndex) {
        rightTabs->getWorkspace(tabIndex)->saveFile();
        rightTabs->getWorkspace(tabIndex)->deleteLater();
        rightTabs->removeTab(tabIndex);
    });

    connect(
        rightTabs,
        &TabWidget::tabBarMiddleMouseClicked,
        [&](int tabIndex) { rightTabs->tabCloseRequested(tabIndex); });

    connect(
        leftTabs, &TabWidget::tabBarMiddleMouseClicked, [&](int tabIndex) {
            leftTabs->tabCloseRequested(tabIndex);
        });

    connect(rightTabs, &TabWidget::newEmptyTabCreated, [&](int index) {
        Workspace* newWorkspace = new Workspace(this);
        newWorkspace->openNewFileMenu();
        rightTabs->resetWidgetAt(newWorkspace, index);
        updateUI();
    });

    connect(leftTabs, &TabWidget::newEmptyTabCreated, [&](int index) {
        Workspace* newWorkspace = new Workspace(this);
        newWorkspace->openNewFileMenu();
        leftTabs->resetWidgetAt(newWorkspace, index);
        updateUI();
    });
}

void DoublePaneTabs::updateUI() {
    if (leftTabs->count() == 0 && rightTabs->count() == 0) {
        leftTabs->addTab(new Workspace(this), "");
    }

    if (leftTabs->count() == 0 && rightTabs->count() != 0) {
        leftTabs->hide();
    } else {
        leftTabs->show();
    }

    if (rightTabs->count() == 0) {
        rightTabs->hide();
    } else {
        rightTabs->show();
    }

    if (rightTabs->count() == 0 && leftTabs->count() == 0) {
        leftTabs->addTab(new Workspace(this), "");
        updateUI();
    }

    for (int i = 0; i < leftTabs->count(); ++i) {
        Workspace* workspace = dynamic_cast<Workspace*>(
            leftTabs->widget(i));
        DEBUG_ASSERT_TRUE(workspace != nullptr);
        if (workspace != nullptr) {
            leftTabs->setTabText(i, workspace->getName());
        }
    }

    for (int i = 0; i < rightTabs->count(); ++i) {
        Workspace* workspace = dynamic_cast<Workspace*>(
            leftTabs->widget(i));
        DEBUG_ASSERT_TRUE(workspace != nullptr);

        rightTabs->setTabText(i, workspace->getName());
    }
}


void DoublePaneTabs::moveToOtherField(
    TabWidget* source,
    TabWidget* destination,
    int        sourceIndex) {
    DPN_FUNC_BEGIN

    QWidget* widget = source->widget(sourceIndex);
    QString  name   = source->tabText(sourceIndex);
    source->removeTab(sourceIndex);
    destination->addTab(widget, name);
    updateUI();

    DPN_FUNC_END
}

void DoublePaneTabs::moveTabToOtherPane(int index) {
    if (selectedPane == leftTabs) {
        moveToOtherField(leftTabs, rightTabs, index);
    } else {
        moveToOtherField(rightTabs, leftTabs, index);
    }
}

void DoublePaneTabs::keyPressEvent(QKeyEvent* event) {
    DPN_FUNC_BEGIN

    KeyEvent keyEvent(event);
    processKeyEvent(keyEvent);
    QWidget::keyPressEvent(event);
    updateUI();

    DEBUG_FINALIZE_WINDOW
    DPN_FUNC_END
}

void DoublePaneTabs::initShortcuts() {
    shortcuts.addBinding("alt+right", "move-tab-to-other-pane");
    keyboardActions["move-tab-to-other-pane"] =
        [&](KBDParameters& internalParameters,
            json&          userParameters) -> bool {
        Q_UNUSED(userParameters)
        Q_UNUSED(internalParameters)

        moveTabToOtherPane(selectedPane->currentIndex());
        return true;
    };
}

bool DoublePaneTabs::processKeyEvent(KeyEvent& event) {
    DPN_FUNC_BEGIN

    QString       command = shortcuts.getCommand(event.getSequence());
    KBDParameters parameters;
    parameters.target = this;
    return executeCommand(command, parameters);

    DPN_FUNC_END
}

bool DoublePaneTabs::passDown(KeyEvent& event) {
    return selectedPane->processKeyEvent(event);
}
} // namespace wgt
