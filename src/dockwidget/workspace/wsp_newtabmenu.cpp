

#include "wsp_newtabmenu.hpp"

WSP_NewTabMenu::WSP_NewTabMenu(QWidget* parent, QFileInfo settingsFile)
    : QWidget(parent) {
    scene  = new QGraphicsScene(this);
    view   = new QGraphicsView(this);
    layout = new QHBoxLayout(this);
    view->setScene(scene);
    layout->addWidget(view);
    readSettings(settingsFile.absoluteFilePath());
    updateUI();
}

WSP_NewTabMenu::~WSP_NewTabMenu() {
    qDeleteAll(buttons);
}

void WSP_NewTabMenu::updateUI() {
    scene->clear();

    for (WSP_NewTabButton* button : buttons) {
        button->updatePos();
    }

    for (WSP_NewTabButton* button : buttons) {
        scene->addItem(button);
    }
}

void WSP_NewTabMenu::readSettings(QString path) {
    DEBUG_SOFT_ASSERT_FILE_EXISTS(path)
    if (!QFileInfo::exists(path)) {
        return;
    }
    json layoutFile = jsonFromFile(path);
    for (json& buttonName : layoutFile["buttons"]) {
        WSP_NewTabButton* button = new WSP_NewTabButton(this);
        button->fromJson(buttonName);
        button->updatePos();
        buttons.push_back(button);
    }
}

void WSP_NewTabMenu::buttonPressed(QString extension) {
    WSP_LOG << "Buton pressed";
    emit fileTypeSelected(extension);
}
