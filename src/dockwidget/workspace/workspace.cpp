

#include "workspace.hpp"

Workspace::Workspace(QWidget* parent) : QWidget(parent) {
    WSP_FUNC_BEGIN
    workspaceState = WorkspaceState::NewTabMenu;
    initUI();
    updateUI();
    WSP_FUNC_END
}


Workspace::~Workspace() {
    WSP_LOG << "Destroying workspace";
}

void Workspace::setIconsDir(const QString& value) {
    iconsDir = value;
}


/*!
 * \brief Set workspace's content into new tab mode. I.e close previously
 * contained widget and display new tab menu
 */
void Workspace::openNewFileMenu() {
    workspaceState = WorkspaceState::NewTabMenu;
    updateUI();
}


/*!
 * \brief Get widget that can open this type of file. Set this workspace as
 * parent workspace for new widget
 * \param extension Extension of the file
 * \return Raw pointer to new widget
 * \todo Return unique_ptr instead of raw pointer
 */
wgtm::WidgetManageable* Workspace::resolveWidget(QString extension) {
    WSP_FUNC_BEGIN

    wgtm::WidgetManageable* result = nullptr;

    WSP_LOG << "Finding widget for file with extension" << extension;
    if (extension == "md") {
        WSP_LOG << "Markdown";
        result = new wgtm::MarkdownEditor(this);

    } else if (extension == "pdf") {
        WSP_LOG << "PDF";
        result = new wgtm::PdfViewer(this);

    } else if (extension == "qslink") {
        WSP_LOG << "QSLink";
        result = new dwgt::LinkViewer(this);

    } else if (extension == "qsnote") {
        WSP_LOG << "QSNote";
        result = new dwgt::NoteViewer(
            "/mnt/workspace/Test/qiucus-development/qwgtlib/settings",
            this);

    } else if (extension == "html") {
        WSP_LOG << "HTML";
        result = new wgtm::RichTextEditor(iconsDir, this);

    } else if (spt::isImageExtension(extension)) {
        WSP_LOG << "IMAGE";
        result = new wgtm::WebView(this);

    } else if (spt::isTextExtension(extension)) {
        WSP_LOG << "TEXT";
        result = new wgtm::CodeEditor(this);

    } else {
        WSP_LOG << "DEFAULT";
        result = new wgtm::CodeEditor(this);
    }

    result->setParentWorkspace(this);

    WSP_FUNC_END

    return result;
}
