

#include "workspace.hpp"

void Workspace::initUI() {
    mainLayout = new QGridLayout(this);
    setLayout(mainLayout);
    newFileMenuLayout
        = "/home/maxim/Documents/qiucus/widgets/dockwidget/workspace/"
          "wsp_newfilemenu_layout.json";
}


// DOC
void Workspace::updateUI() {
    WSP_FUNC_BEGIN

    if (workspaceState == WorkspaceState::FileOpened) {
        setToOpenedFileMode();
    } else if (workspaceState == WorkspaceState::NewTabMenu) {
        setToNewFileMode();
    }

    WSP_FUNC_END
}


/// \todo Close contained widget and save it's content
void Workspace::setToNewFileMode() {
    WSP_FUNC_BEGIN


    if (containedWidget != nullptr) {
        WSP_LOG << "Removing old widget";
        //        mainLayout->removeWidget(containedWidget);
        //        containedWidget->deleteLater();
        //        containedWidget = nullptr;
        containedWidget->hide();
    }

    if (newTabMenu == nullptr) {
        WSP_LOG << "Creating newFileMenu";
        newTabMenu = new WSP_NewTabMenu(this);
    }

    WSP_LOG << "Setting up signals";
    connect(
        newTabMenu,
        &WSP_NewTabMenu::fileTypeSelected,
        this,
        &Workspace::newFile,
        Qt::UniqueConnection);


    mainLayout->addWidget(newTabMenu);
    newTabMenu->show();
    workspaceState = WorkspaceState::NewTabMenu;


    WSP_FUNC_END
}


// DOC
void Workspace::setToOpenedFileMode() {
    WSP_FUNC_BEGIN
    DEBUG_ASSERT_TRUE(widgetHandle.get() != nullptr);


    if (workspaceState == WorkspaceState::NewTabMenu) {
        WSP_LOG << "Switching from new file menu";
        newTabMenu->hide();
        workspaceState = WorkspaceState::FileOpened;
        setToOpenedFileMode();

    } else if (workspaceState == WorkspaceState::FileOpened) {
        WSP_LOG << "Updating opened widget";

        QWidget* newContainedWidget = dynamic_cast<QWidget*>(
            widgetHandle.get());
        DEBUG_ASSERT_TRUE(newContainedWidget != nullptr)

        newTabMenu->hide();

        if (containedWidget != nullptr) {
            WSP_LOG << "Deleting previous widget from layout";
            mainLayout->removeWidget(containedWidget);
        } else {
            WSP_LOG << "No widget in layout";
        }

        containedWidget = newContainedWidget;
        containedWidget->setParent(this);

        containedWidget->setMinimumSize(0, 0);
        mainLayout->addWidget(containedWidget);
    }


    WSP_FUNC_END
}
