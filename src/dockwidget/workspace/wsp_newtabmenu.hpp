#ifndef WORKSPACENEWFILE_HPP
#define WORKSPACENEWFILE_HPP

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QHBoxLayout>
#include <QWidget>

#include <algorithm>
#include <memory>
#include <vector>

#include <hsupport/qjson.hpp>

#include "wsp_debug_macro.hpp"
#include "wsp_newtabbutton.hpp"


class WSP_NewTabMenu : public QWidget
{
    Q_OBJECT
  public:
    explicit WSP_NewTabMenu(
        QWidget*  parent       = nullptr,
        QFileInfo settingsFile = QFileInfo());
    ~WSP_NewTabMenu();

  private:
    QGraphicsScene* scene;
    QGraphicsView*  view;
    QHBoxLayout*    layout;
    void            updateUI();
    void            readSettings(QString path);

    std::vector<WSP_NewTabButton*> buttons;


  signals:
    QString fileTypeSelected(QString extension);

  public slots:
    void buttonPressed(QString extension);
};

#endif // WORKSPACENEWFILE_HPP
