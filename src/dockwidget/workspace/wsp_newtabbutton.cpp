

#include "wsp_newtabbutton.hpp"
#include "wsp_newtabmenu.hpp"

WSP_NewTabButton::WSP_NewTabButton(WSP_NewTabMenu* _menu)
    : menu(_menu) {
    setAcceptHoverEvents(true);
}

QString WSP_NewTabButton::getButtonName() const {
    return buttonName;
}

void WSP_NewTabButton::setButtonName(const QString& value) {
    buttonName = value;
}

uint WSP_NewTabButton::getRow() const {
    return row;
}

void WSP_NewTabButton::setRow(const uint& value) {
    row = value;
}

void WSP_NewTabButton::updatePos() {
    this->setPos(pos);
}

uint WSP_NewTabButton::getColumn() const {
    return column;
}

void WSP_NewTabButton::setColumn(const uint& value) {
    column = value;
}

void WSP_NewTabButton::fromJson(json& settings) {
    pos.setX(settings["posX"]);
    pos.setY(settings["posY"]);
    buttonRect.setHeight(settings["height"]);
    buttonRect.setWidth(settings["width"]);
    buttonName = QString::fromStdString(settings["name"]);
    extension  = QString::fromStdString(settings["extension"]);

    QFont         font;
    QFontMetricsF fontMetrics(font);
    textSize.setHeight(fontMetrics.height());
    textSize.setWidth(fontMetrics.width(buttonName));

    updatePos();
}


QRectF WSP_NewTabButton::boundingRect() const {
    return buttonRect;
}

void WSP_NewTabButton::paint(
    QPainter*                       painter,
    const QStyleOptionGraphicsItem* option,
    QWidget*                        widget) {
    Q_UNUSED(option)
    Q_UNUSED(widget);
    QPen pen;
    pen.setWidth(3);
    if (isHovered) {
        pen.setColor(Qt::red);
    } else {
        pen.setColor(Qt::black);
    }
    painter->setPen(pen);
    painter->drawRect(buttonRect);
    painter->drawText(
        buttonRect.width() / 2 - textSize.width() / 2,
        buttonRect.height() / 2 + textSize.height() / 2,
        buttonName);
}

void WSP_NewTabButton::hoverEnterEvent(QGraphicsSceneHoverEvent* event) {
    isHovered = true;
    QGraphicsItem::hoverEnterEvent(event);
}

void WSP_NewTabButton::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) {
    isHovered = false;
    QGraphicsItem::hoverLeaveEvent(event);
}

void WSP_NewTabButton::mousePressEvent(QGraphicsSceneMouseEvent* event) {
    menu->buttonPressed(extension);
    QGraphicsItem::mousePressEvent(event);
}
