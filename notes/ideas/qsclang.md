<!-- MarkdownTOC autolink="true" levels="1,2,3,4,5,6" bracket="round" style="unordered" indent="    " autoanchor="false" -->

- [Note](#note)
- [What do i want to include in this language](#what-do-i-want-to-include-in-this-language)
    - [Features from other languages](#features-from-other-languages)
    - [Own features](#own-features)
    - [Examples of code](#examples-of-code)
        - [Filter forks](#filter-forks)
        - [Batch convert images](#batch-convert-images)
        - [Regex search for file containing](#regex-search-for-file-containing)
        - [Move all files in directory](#move-all-files-in-directory)
        - [Read all files in directory, filter images and copy each one in different directory if it has size more than 4k](#read-all-files-in-directory-filter-images-and-copy-each-one-in-different-directory-if-it-has-size-more-than-4k)
        - [Basic image viewer with button and text](#basic-image-viewer-with-button-and-text)
        - [Get images from URL](#get-images-from-url)
- [Syntax](#syntax)
- [Types](#types)
    - [Basic types](#basic-types)
    - [Typeclasses](#typeclasses)
    - [Object classes](#object-classes)
        - [Predefined](#predefined)
- [Control blocks](#control-blocks)
    - [If](#if)
    - [While](#while)
- [Functions](#functions)
    - [Function declaration and call](#function-declaration-and-call)
    - [Pattern matching](#pattern-matching)
    - [Lambda](#lambda)
- [Objects](#objects)
    - [Object initialization](#object-initialization)
        - [Description initialization](#description-initialization)
    - [Object definition](#object-definition)
        - [Regular object](#regular-object)
        - [Abstract object](#abstract-object)
        - [Inheritance](#inheritance)

<!-- /MarkdownTOC -->

## Note 

This is DSL language designed for use in QIUCUS software. It is just high-level command tool for some tasks.

## What do i want to include in this language

### Features from other languages

+ Weak static typing
+ Patter mathcing
+ Lambda functions
+ Funciton composition
+ Pipes (from bash)
+ Rebol interface tools
+ Easy regex
+ Json object description when initializing
+ Explicit constructors

### Own features

+ Lambda classes


### Examples of code

#### Filter forks

```
Dir::read("~/Downloads/test/")
|>  Fork
    !>  filter(&, extension == "txt")
          |> print(File::extension(&))
    !>  filter(&, extension == "md")
          |> File::build(&)
    !>  filter(&, extension == "jp2")
          |> File::convert(&, File::image)
          |> Image::export(&, format="tga")

```

#### Batch convert images

```
// By default all futher operations that requre folder to operate will 
// use this folder if other option is not specified )it can be done with
// "default=false" option
dir.read("~/Downloads/test/") 
 |> filter(&, extension == "jpg")
 |> File::convert(&, File::image)
 |> Image::export(&, format="tga")
```

#### Regex search for file containing

```
dir.read(/home/name/documents)
 |> filter(&, is_text)
 |> filter(&, Document)
 |> filter(&, contains(Regex(SR = true) : {
      begin with any of (digit, letter, one of "._%+-") once or more,
      literally "@",
      any of (digit, letter, one of ".-") once or more,
      literally ".",
      letter at least 2 times,
      must end, case insensitive
    }))
 |> print(&)
```

1. Read all files in directory `/home/name/documents`
2. Filter files that are text files
3. Convert them to Document objects
4. For each Document check if contains email address
5. Print mathcing files

#### Move all files in directory

+ Move all files to directory
+ If mathcing filenames found compare files
  + If Files has the same name but content does not match generate new random name for new file
  + If file match ignore collision
  + If Extension comparison is not failed ask for resolution

```
old_path = "/home/name/documents" 
// Type will be automatically deducted from parameter type
new_path = "/home/name/documents/collection"
dir.read(old_path)
 |> File::safe_move(&, new_path) 
// Return file names that give errors
 |> for name in &:
    if not File::supported(name):
      ask_name = new View -> string {
        main_layout = new VBox [
          button_ok : new Button {
            size : 200x30,
            text : "Rename",
            on_clicked : view.finish()
          },
          new_name : new TextEditLine {
            size :
          }
        ]
        slots : [
          finish() {
            return new_name.text()
          }
        ]
      }
      File::move(old_path + name, new_path + ask_name.exec())
    if File::compare(old_path + name, new_path + name):
      // Files are considered equal
      // Also possible to use fuzzy compare
    else:
      // Files are not equal and this extension is familliar to us
      name += String::random(10) // We might check again, but this is not neccesaryname
      File::move(old_path + name, new_path + ask_name.exec())
```
  
#### Read all files in directory, filter images and copy each one in different directory if it has size more than 4k

```
dir.read(/home/name/pictures) 
 |> filter(&, is_image) 
 |> convert(&, Image) 
 |> filter(&, size > "4k") 
 |> File::copy_to(&, /home/name/new_folder) 
// If two files collide old one will be overwritten
```

1. Read all files in directory `/home/name/pictures`
2. For each item check if it is an image file
3. If each file construct temporary image object
4. For each object call funciton `size()` and compare it with `4k`
5. For each image copy it to folder `/home/name/new_folder` if such folder do not exist, create new one

#### Basic image viewer with button and text

```
image_resolver = new View : {
  main_layout : new VBox[
    image : new Image {
      size : 210x200,
    },
    buttons : new 
  ]
  below across : [
    button : {
      clicked : emit new_image,
      double-clicked : emit previous_image
    },
    text : "Hello motherfucker"
  ]

  slots : [
    load_image (string path) : {
      image.load(path)
    }
  ]

  def init:
    connect this button.new_image 
    // This function REQURES at least four parameters and will 
    // consume everythin until they are filled
            this load_image("/some/default/path") 
}

```


All text before the header will be treated as comment, as long as you avoid
using the word "red" starting with a capital "R" in this pre-header text.
This is a temporary shortcoming of the used lexer but most of the time you
start your script or program with the header itself.

The header of a red script is the capitalized word "red" followed by a
whitespace character followed by a block of square brackets []. The block of
brackets can be filled with useful information about this script or program:
the author's name, the filename, the version, the license, a summary of what
the program does or any other files it needs. The red/System header is just
like the red header, only saying "red/System" and not "red".

If no such comment is nessessary it can be omitted and execution will start immediately


```
// this is a commented line
print "Hello Red World"

/*
    This is a multiline comment.
    You just saw the Red version of the "Hello World" program.
*/
```

Your program's entry point is the first executable code that is found no need to restrict this to a 'main' function.

Valid characters in variable names: `a-z A-Z _ '`

Variable declaration is not nessessary but should be used (syntax analyzer warninings)

#### Get images from URL

1. Get all images from URL
2. Filter ones that are more than certain size

```
Web::Html page = Web::html_from_url("Insert url here")
page.get_images()
 |> filter(&, dimensions >= 300x300)
 |> File::save_new(&) 
 // Will try to save all images in working directory of a script and 
 // resolve all file name conflicts

```


## Syntax

| Letter | Meaning                                            |
|:-------|:---------------------------------------------------|
| `.\`   | Start of lambda function                           |
| `;`    | End of expression line (optional)                  |
| `{}`   | Start/end of code block                            |
| `==`   | Comparison                                         |
| `<=`   | A less or equal to B                               |
| `>=`   | A more or equal to B                               |
| `<`    | A less than B                                      |
| `>`    | A more than B                                      |
| `~=`   | Approximately equal                                |
| `!=`   | A not equal to B                                   |
| pipe   | Map container from first result to second function |
| ` . `  | Funciton composition                               |
| `&`    | Pipe's output result                               |
| `and`  |                                                    |
| `or`   |                                                    |
| `xor`  |                                                    |
| `not`  |                                                    |

## Types

### Basic types

| Type   | Description             |
|:-------|:------------------------|
| int    | 8-bit integer           |
| float  | floating point variable |
| double | double precision float  |
| bool   | Boolean variable        |
|        |                         |

### Typeclasses

| Type | Description                          |
|:-----|:-------------------------------------|
| Comp | Comparison         `==,<,>,<=,>=,~=` |
| Arr  | Container types                      |

### Object classes

#### Predefined

| Type     | Description          |
|:---------|:---------------------|
| Document | Text file or string  |
| Image    | Image file or pixmap |
| View     | GUI window           |
| Widget   | GUI widget           |
| Regex    | Regular expression   |
| String   | UNICODE char string  |
| Numeric  | Numeric data types   |


## Control blocks

### If

```
if expression:
  // Code
elif:
  // Code
else:
  // Code
```

### While

```
while condition:
  // Code

```


## Functions

### Function declaration and call

```
def example(int first = 1, int second = 2): -> int
  // Some code

int b_1 = example(1,2)
int b_2 = example . example 1 3 // 1
int b_2 = example . example (second = 3) // 1
int b_2 = example(example (second = 3)) // 1
```

Function that takes two int parameters and returns int value. Lines commented as `// 1` are identical. By default funciton will treat all values after call as parameters until all parameter slots are busy. If less than max is found but end of block is reached function will be called wil default parameters (if available). Operator ` . ` is used for function composition

```
def void(int a) -> void:
  // summ_something

def else(int a): // Function with no return types considered void function
  // Code

```

### Pattern matching

```
def example_2(x:float): -> float
  | x == 2 return (x^2)
  | x == 3 return (x^3)
  | x == 4
    int temp = 89
    temp += temp ** sqrt(x)
    return temp
  | return x^90 // Default value
```

### Lambda

```
int x = 90
some_variable = .\  (): int temp; temp = x^90; return temp;

```

## Objects

### Object initialization

```
name_of_object = new Object // Default object initialization
name_of_object = new Object(12,23) // Regular object initialization
name_of_object = new Object(useless_parameter = 23) {
  image_name : new WImage {

  }
} // Description initialization
```

#### Description initialization

Similar to lambda functions we can create anonymous object instance with new functions and parameters

__Syntax__

```
object_name = new Object { 
  // Curly braces show that this is not a regular Object initialization
  //but a new object type generated at runtime
  image : WImage {
    // This is regular description for object with named 
    // parameter initialization
  }
}

```

### Object definition

#### Regular object

```
class TestObject:
  private:
    int test = 0
    static int temo = 90

  public:
    def TestObject(float value); // Empty block
    def TestObject(int value):   // Constructor definition
      test = value

    static def compare(TestObject temp_1, TestObject temp_2):
      return temp_1.

    def operator== (TestObject temp_2):
      return compare(this, temp_2)

TestObject::TestObject(float value):
  this.test = int(value)

```

+ Static variables
+ Static functions
+ Postponed definition of a function

#### Abstract object

```
abstract class Abstract:
  public:
    virtual def randomEmptyFunction(int b = 2) = None; 
    // Empty virtual funciton with no definition but with default parameters
```

+ Abstract classes
+ No constructor == empty default constructor (if all memebers have default constructors)

#### Inheritance

```
class Material(Abstract):
  public:
    override def randomEmptyFunction(int b = 3):
      print(b)
    // Override virtual function and it's default parameter
```
