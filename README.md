![image](data/coding_drunk.png)

_Totally unrelated to code quality_

---

**WARNING**: *Not final project, right now this is only proof of concept, so bugs, segfaults, bad design etc are common*

**IMPORTANT**: Even though this project is in development I try to make my code as modular as possible, which means that there are some things that you might want to use without touching all project (especially in `test` and `widgets` folders)



**NOTE**: There is [doxygen](http://www.stack.nl/~dimitri/doxygen/) documentation and [sourcetrail](https://www.sourcetrail.com/) project, they might help you to understant code a little more.

---

This is a list of features that are already implemented

**NOTE**: For the most part they are in incomplete state and will be improved later

- Editing basic markdown files in real time
- Rich text files in html format
- PDF viewer based on Qt poppler
- Code editor widget based on scintilla


---

## Dependencies

+ Boost
+ Qt 5.11.1
+ [QScintilla](http://pyqt.sourceforge.net/Docs/QScintilla2/)
+ [Poppler qt module](https://en.wikipedia.org/wiki/Poppler_(software))

## Building

### Source

```bash
git clone --recurse-submodules https://gitlab.com/haxscramper/Qiucus_development.git Qiucus-dev
```

### QScintilla

```bash
wget "https://sourceforge.net/projects/pyqt/files/QScintilla2/QScintilla-2.10.7/QScintilla_gpl-2.10.7.tar.gz"
tar -xvf QScintilla_gpl-2.10.7.tar.gz
cd QScintilla_gpl-2.10.7
cd Qt4Qt5
qmake
make
cd ..
mv Qt4Qt5 Qiucus-dev

# Optional

rm -r QScintilla_gpl-2.10.7
rm QScintilla_gpl-2.10.7.tar.gz
```

### Qiucus

```bash
cd Qiucus-dev
mkdir build
cd build
qmake ../Qiucus
make --quiet
```
