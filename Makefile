msg := $(shell  bash -c "((colecho > /dev/null && echo 'colecho -v ') \
2> /dev/null || echo 'echo -e \"\\033[91m[========]\033[0m: \"-- ')")

none:
	@$(msg) -e0 "You need to specify target explicitly"

##= All

rebuild-all: clean-all build-all


build-all: build-qiucus

clean-all:
	@rm -rf build

##= Qiucus

build-qiucus:
	@$(msg) -i3 "qiucus-desktop build"
	@mkdir -p build/qiucus
	@{ cd build/qiucus ; qmake -r -Wnone ../../src/qiucus/qiucus.pro ; make -s -j10 ; }


clean-qiucus:
	@rm -rf build/qiucus
